# Copyright Michele Vidotto 2013 <michele.vidotto@gmail.com>

# assembly evaluation with BUSCO

# TODO:

PROFILE_URL ?= http://busco.ezlab.org/files/vertebrata_buscos.tar.gz
PROFILE_CLADE ?= vertebrates

logs:
	mkdir -p $@

assembly.fasta:
	ln -sf $(ASSEMBLY) $@

$(PROFILE_CLADE):
	mkdir -p $@ \
	&& wget -qO- $(PROFILE_URL) \
	| tar -xzf - --strip-components=1 -C $@

run_results: assembly.fasta $(PROFILE_CLADE)
	!threads
	$(call load_modules); \
	BUSCO_v1.1b1.py \
	-in $< \
	-o results \
	-f \
	-l vertebrata \
	-c $$THREADNUM \
	-m trans

# python BUSCO_v1.1b.py -o NAME -in TRANSCRIPTOME -l LINEAGE -m trans


.PHONY: test
test:
	@echo



ALL += logs \
	assembly.fasta \
	$(PROFILE_CLADE) \
	run_results


INTERMEDIATE += 

CLEAN += temp