# Copyright 2014 Michele Vidotto <michele.vidotto@gmail.com>

# TODO:

# Values optimized for SMALT aligner
MIN_INSERT ?= 100 
MAX_INSERT ?= 500
REPETITIVE_MAX_QUAL ?= 3
PERFECT_MIN_QUAL ?= 4
PERFECT_MIN_ALIGNMENT_SCORE ?= 76

ASSEMBLY ?=
REVERSE_LONG_INSERT ?= yes

# log dir
logs:
	mkdir -p $@

# this function install all the links at once
.PHONY: link_install
link_install:
	@echo installing links ... \
	$(foreach SAMPLE,$(LONG_INSERT_LIBS) $(SHORT_INSERT_LIBS), \
		$(foreach KEY,$(call keys,$(SAMPLE)), \
			$(shell ln -sf $(call get,$(SAMPLE),$(KEY)) $(SAMPLE).$(KEY).fastq.gz) \
		) \
	)

# cat all long insert read 1 or 2 into single file
# A minimum of ∼15X fragment coverage is needed
# Use the largest insert data available
LONG_INSERT.%.fastq:
	@echo -e "merge to $@ ...\n" \
	$(shell >$@)
	$(foreach SAMPLE,$(LONG_INSERT_LIBS), \
		$(shell zcat $(call get,$(SAMPLE),$*) >>$@) \
	)

ifeq ($(REVERSE_LONG_INSERT),yes)
# The correct orientation of these reads should be "inwards" so
# reverse complement the mate-pairs that are "outward"
reverse.LONG_INSERT.%.fastq: LONG_INSERT.%.fastq
	$(call load_modules); \
	fastx_reverse_complement -i $< -o $@ -v -Q33
else
reverse.LONG_INSERT.%.fastq: LONG_INSERT.%.fastq
	ln -sf $< $@
endif

# cat all short insert read 1 or 2 into single file

# short insert Illumina paired that maps perfectly and uniquely to the assembly allow REAPR 
# to accurately call error-free bases in the assembly. It will not affect
# the error calling.
# The correct orientation of these reads should be inwards.
# At least 5X perfect and unique coverage is needed for these reads
SHORT_INSERT.%.fastq:
	@echo -e "merge to $@ ...\n" \
	$(shell >$@)
	$(foreach SAMPLE,$(SHORT_INSERT_LIBS), \
		$(shell zcat $(call get,$(SAMPLE),$*) >>$@) \
	)


assembly.fasta:
	zcat <$(ASSEMBLY) >$@


# sanitize sequence name in assembly.fasta by replacing ‘bad’ characters with an underscore
sanitized.assembly.fa: assembly.fasta
	$(call load_modules); \
	reapr facheck $< || { reapr facheck $< $(basename $@); exit 0; }; \   * produce new sanitized.assembly.fa if bed characters are found, otherwise link the prerequisite *
	ln -sf $< $@


# map with smalt. The output is a sorted and indexed BAM file with duplicates removed.
long_mapped.bam: logs sanitized.assembly.fa reverse.LONG_INSERT.1.fastq reverse.LONG_INSERT.2.fastq
	!threads
	$(call load_modules); \
	reapr smaltmap -n $$THREADNUM $^2 $^3 $^4 $@ \
	2>&1 \
	| tee $</reapr_smaltmap.$@.log


short_mapped.bam: logs sanitized.assembly.fa SHORT_INSERT.1.fastq SHORT_INSERT.2.fastq
	!threads
	$(call load_modules); \
	reapr smaltmap -n $$THREADNUM $^2 $^3 $^4 $@ \
	2>&1 \
	| tee $</reapr_smaltmap.$@.log

# The "large genome" strategy is used in order to limit the use of memory

# The BAM file will be filtered, so that only paired reads pointing towards each other, within
# the given insert size range and with at least the given mapping quality and alignment score
# are included

# options: <min insert> <max insert> <repetitive max qual> <perfect min qual> <perfect min alignment score>
perfect.short_mapped.hist: logs short_mapped.bam
	$(call load_modules); \
	reapr perfectfrombam $^2 $(basename $@) $(MIN_INSERT) $(MAX_INSERT) $(REPETITIVE_MAX_QUAL) $(PERFECT_MIN_QUAL) $(PERFECT_MIN_ALIGNMENT_SCORE) \
	2>&1 \
	| tee $</reapr_perfectfrombam.$@.log

# run the reapr pipeline: facheck, preprocess, stats, fcdrate, score, break and summary
05.summary.report.txt: logs sanitized.assembly.fa long_mapped.bam perfect.short_mapped.hist
	$(call load_modules); \
	reapr pipeline $^2 $^3 reapr_pipeline $(basename $^4) \
	2>&1 \
	| tee $</reapr_pipeline.$@.log \
	&& ln -sf reapr_pipeline/$@ .


.PHONY: test
test:
	@echo

ALL +=  logs \
	link_install \
	assembly.fasta \
	sanitized.assembly.fa \
	long_mapped.bam \
	short_mapped.bam \
	perfect.short_mapped.hist \
	05.summary.report.txt
	

INTERMEDIATE += 

CLEAN += $(wildcard reapr_pipeline*) \
	 $(wildcard *.fastq*) \
	 $(wildcard *.bai) \
	 $(wildcard perfect.short_mapped.*) \
	 sanitized.assembly.fa.fai